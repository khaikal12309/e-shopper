-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Jan 2020 pada 04.15
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_shopper`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_category`
--

CREATE TABLE `tb_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `descripsi` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_category`
--

INSERT INTO `tb_category` (`id`, `name`, `descripsi`) VALUES
(1, 'Elektronik', 'Kategori Elektronik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_coment`
--

CREATE TABLE `tb_coment` (
  `id` int(11) NOT NULL,
  `user_id` int(200) NOT NULL,
  `transaksi_id` int(200) NOT NULL,
  `coment` varchar(250) DEFAULT NULL,
  `rate` int(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_delivery`
--

CREATE TABLE `tb_delivery` (
  `id` int(11) NOT NULL,
  `transaksi_id` int(100) NOT NULL,
  `no_resi` varchar(100) NOT NULL,
  `kurir` varchar(100) NOT NULL,
  `status` enum('draft','pending','done') NOT NULL DEFAULT 'draft',
  `tgl_kirim` datetime DEFAULT NULL,
  `tgl_sampai` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_delivery`
--

INSERT INTO `tb_delivery` (`id`, `transaksi_id`, `no_resi`, `kurir`, `status`, `tgl_kirim`, `tgl_sampai`) VALUES
(2, 4, '10103021', 'JNT', 'pending', '2020-01-17 23:40:05', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_transaksi`
--

CREATE TABLE `tb_detail_transaksi` (
  `id` int(11) NOT NULL,
  `product_id` int(100) NOT NULL,
  `qty` int(100) NOT NULL,
  `price_total` float NOT NULL,
  `transaksi_id` int(100) NOT NULL,
  `status` enum('draft','pending','done') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_paid`
--

CREATE TABLE `tb_paid` (
  `id` int(11) NOT NULL,
  `transaksi_id` int(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `status` enum('draft','pending','done') NOT NULL DEFAULT 'draft',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_paid`
--

INSERT INTO `tb_paid` (`id`, `transaksi_id`, `image`, `status`, `date`) VALUES
(0, 4, 'img_.jpg', 'done', '2020-01-17 15:02:37'),
(3, 3, 'img_.png', 'draft', '2020-01-17 14:58:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_product`
--

CREATE TABLE `tb_product` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(200) NOT NULL,
  `qty` int(50) NOT NULL,
  `category_id` int(200) NOT NULL,
  `descripsi` varchar(200) DEFAULT NULL,
  `price` int(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_product`
--

INSERT INTO `tb_product` (`id`, `name`, `image`, `qty`, `category_id`, `descripsi`, `price`) VALUES
(1, 'Samsung j5 Pro', 'img_Samsung_j5_Pro.jpg', 20, 1, '-Warna : Gold', 1500000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_toko`
--

CREATE TABLE `tb_toko` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `transfer_rek` varchar(255) NOT NULL,
  `phone` float DEFAULT NULL,
  `descripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_toko`
--

INSERT INTO `tb_toko` (`id`, `name`, `address`, `transfer_rek`, `phone`, `descripsi`) VALUES
(0, 'TOko E Shopper', 'JL Raya Binong', 'BRI : 9980 - 900 - 3321', 2690000, 'Toko Terbaik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `id` int(11) NOT NULL,
  `user_id` int(200) NOT NULL,
  `product_id` int(200) NOT NULL,
  `qty` int(200) NOT NULL,
  `price_total` int(150) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `delivery_ship` varchar(200) DEFAULT NULL,
  `status` enum('draft','pending','done') NOT NULL DEFAULT 'draft'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`id`, `user_id`, `product_id`, `qty`, `price_total`, `date`, `delivery_ship`, `status`) VALUES
(4, 17, 1, 20, 30000000, '2020-01-17 15:02:09', 'OKO', 'draft');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` enum('admin','user') NOT NULL DEFAULT 'user',
  `image` varchar(200) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `phone` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `name`, `password`, `role`, `image`, `address`, `email`, `phone`) VALUES
(1, 'Admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', NULL, NULL, 'admin', 0),
(17, 'Dani', '4297f44b13955235245b2497399d7a93', 'user', 'img_Dani_Alves.jpg', 'Kp Kalapa Dua', 'danies@gmail.com', 890000),
(19, 'Khaikal', '98dd3a40a936fceb71c88ac01e3c472b', 'user', 'img_Khaikal.jpg', 'Kp Kalapa Dua', 'khaikalaz3@gmail.com', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_category`
--
ALTER TABLE `tb_category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_coment`
--
ALTER TABLE `tb_coment`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_delivery`
--
ALTER TABLE `tb_delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_detail_transaksi`
--
ALTER TABLE `tb_detail_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_paid`
--
ALTER TABLE `tb_paid`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_toko`
--
ALTER TABLE `tb_toko`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_category`
--
ALTER TABLE `tb_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_coment`
--
ALTER TABLE `tb_coment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_delivery`
--
ALTER TABLE `tb_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_detail_transaksi`
--
ALTER TABLE `tb_detail_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_paid`
--
ALTER TABLE `tb_paid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_product`
--
ALTER TABLE `tb_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_toko`
--
ALTER TABLE `tb_toko`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
