<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeuser extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('product_model');
        $this->load->model('category_model');
        $this->load->model('transaksi_model');
        $this->load->helper('url');
        $this->load->model('admin');
        $this->load->library('cart');
		if($this->admin->is_role() != "user")
        {
            $this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
            redirect("Welcome");
        }
    }
	public function index()
	{
		$data['products'] = $this->product_model->getAll();
		$data['categorys'] = $this->category_model->getAll();
		$this->load->view('home', $data);
	}

	public function checkout(){
		$transaksi = $this->transaksi_model;
        $transaksi->save();
        $this->cart->destroy();
        redirect('user/homeuser');
	}

    public function tampil_checkout(){
        $user_id = $this->session->userdata("user_id");

        $data['transaksis'] = $this->transaksi_model->getUser($user_id);

        $this->load->view('checkout', $data);
    }

	public function show($id)
	{
		$data['products'] = $this->product_model->getByCat($id);
		$data['category'] = $this->category_model->getById($id);
		$data['categorys'] = $this->category_model->getAll();
		$this->load->view('home2', $data);
	}

	public function tambah_keranjang($id)
	{
		$product = $this->product_model->getById($id);

		$data = array(
	        'id'      => $product->id,
	        'qty'     => 1,
	        'price'   => $product->price,
	        'name'    => $product->name,
			);
		$this->cart->insert($data);
		redirect('user/homeuser');
	}

	public function tampil_keranjang()
	{
		$this->load->view('keranjang');
	}

	function show_cart(){ //Fungsi untuk menampilkan Cart
        $output = '';
        $no = 0;
        foreach ($this->cart->contents() as $items) {
            $no++;
            $output .='
                <tr>
                    <td class="cart_product"><input type="hidden" value="'.$items['id'].'" name="product_id"/>'.$items['name'].'</td>
                    <td class="cart_quantity"><input type="hidden" value="'.$items['qty'].'" name="qty"/>'.$items['qty'].'</td>
                    <td class="cart_total"><input type="hidden" value="'.number_format($items['subtotal']).'" name="price_total"/>'.number_format($items['subtotal']).'</td>
                    <td class="cart_product"><button type="button" id="'.$items['rowid'].'" class="hapus_cart btn btn-danger btn-xs">Batal</button></td>
                </tr>
            ';
        }
        $output .= '
            <tr>
            <td></td>
            <td></td>
                <td><b>Total</b></td>
                <td><b><input type="hidden" value="'.number_format($this->cart->total()).'" name="total"/>'.number_format($this->cart->total()).'</b></td>
            </tr>
        ';
        return $output;
    }
 
    function load_cart(){ //load data cart
        echo $this->show_cart();
    }
 
    function hapus_cart(){ //fungsi untuk menghapus item cart
        $data = array(
            'rowid' => $this->input->post('row_id'), 
            'qty' => 0, 
        );
        $this->cart->update($data);
        echo $this->show_cart();
    }

    public function show_checkout(){
        $data['transaksis'] = $this->transaksi_model->getAll();
        $this->load->view('checkout', $data);
    }

	public function login()
	{
		$this->load->view('login');
	}
}
