<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
    }

    public function index(){
        $this->load->view('admin/login');
    }

    public function do_login()
    {

        if($this->admin->is_logged_in())
            {
                //jika memang session sudah terdaftar, maka redirect ke halaman sesuai session
                if($this->session->userdata("role") == "admin")
                    {
                        redirect('admin/home/');
                    }elseif($this->session->userdata("role") == "user")
                        {
                            redirect('user/homeuser/');
                        }
            
            }
            else
            {
                //get data dari FORM
                $email = $this->input->post("email");
                $password = MD5($this->input->post('password'));

                //checking data via model
                $checking = $this->admin->check_login('tb_user', array('email' => $email), array('password' => $password));
                
                //jika ditemukan, maka create session
                if ($checking != FALSE) 
                    {
                        foreach ($checking as $apps) 
                            {
                                $session_data = array(
                                    'user_id'   => $apps->id,
                                    'user_phone' => $apps->phone,
                                    'user_pass' => $apps->password,
                                    'user_name' => $apps->name,
                                    'user_email' => $apps->email,
                                    'role'      => $apps->role,
                                    'user_address'    => $apps->alamat,
                                );
                                //set session userdata
                                $this->session->set_userdata($session_data);

                                //redirect berdasarkan level user
                                if($this->session->userdata("role") == "admin")
                                    {
                                        $this->session->set_flashdata('success', 'Selamat datang '.$this->session->userdata("user_name").'');
                                        redirect('admin/home/');
                                    }
                                elseif($this->session->userdata("role") == "user")
                                    {
                                        $this->session->set_flashdata('success', 'Selamat datang '.$this->session->userdata("user_name").'');
                                        redirect('user/homeuser/');
                                    }              
                            }
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'Email atau Password salah');
                        $this->load->view('admin/login');
                    }

            }

    }

    public function logout()
    {
        if($this->session->userdata("role") == "admin")
            {
                $this->session->sess_destroy();
                redirect('login');
            }
            elseif($this->session->userdata("role") == "user")
            {
                $this->session->sess_destroy();
                redirect('welcome');
            }
        
    }

    public function setting()
    {
        $this->load->view('change_password');
    }

    public function updatepass($id_user){

        $old_password = MD5($this->input->post('old_password'));
        $new_password = MD5($this->input->post('new_password'));
        $confirm_password = MD5($this->input->post('confirm_password'));

        $user_ini = $this->user_model->getById($id_user);

        if ($old_password != $user_ini->password) {
            $this->session->set_flashdata('error', 'Password lama anda tidak sesuai');
            redirect('admin/Setting');

        }elseif ($new_password != $confirm_password) {
            $this->session->set_flashdata('error', 'Password lama anda tidak sesuai');
            redirect('admin/Setting');
        }else{
            $user['password'] = MD5($this->input->post('new_password'));
 
            $query = $this->user_model->update($user, $id_user);
     
            if($query == TRUE){
                $this->session->set_flashdata('success', 'Password baru anda adalah '.$this->input->post('new_password').'');
                redirect('Login/Setting');
            }
        }
    }

    public function about()
    {
        $this->load->view("about");       

    }
}