<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
        $this->load->model('user_model');
        $this->load->model('product_model');
        $this->load->model('transaksi_model');
        //cek session dan level user
        if($this->admin->is_role() != "admin")
        {
            $this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
            redirect("Welcome");
        }
    }


	public function index()
	{
        $data['count_product']= count($this->product_model->getAll());
        $data['count_user']= count($this->user_model->getUser());
        $data['count_unconfir']= count($this->transaksi_model->getUn());
        $data['count_success']= count($this->transaksi_model->getSuc());

		$this->load->view('admin/home',$data);
	}
}
