<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
        $this->load->model('transaksi_model');
        $this->load->model('user_model');
        $this->load->model('product_model');
        $this->load->helper('url');
        //cek session dan level user
        if($this->admin->is_role() != "admin")
        {
            $this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
            redirect("Welcome");
        }
    }


	public function index()
	{
        $data['transaksis'] = $this->transaksi_model->getAll();
        $data['users'] = $this->user_model->getUser();
        $data['products'] = $this->product_model->getAll();
		$this->load->view('admin/transaksis/index', $data);
	}

    public function create()
    {
        $transaksi = $this->transaksi_model;
        $transaksi->save();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        redirect('admin/transaksi');
        
    }

    public function edit()
    {
        $transaksi = $this->transaksi_model;

        $transaksi->update();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        

        redirect('admin/transaksi');
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->transaksi_model->delete($id)) {
            $this->session->set_flashdata('success', 'Berhasil dihapus');
            redirect('admin/transaksi');
        }
    }

    
}