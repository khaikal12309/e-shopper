<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
        $this->load->model('product_model');
        $this->load->model('category_model');
        $this->load->helper('url');
        //cek session dan level user
        if($this->admin->is_role() != "admin")
        {
            $this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
            redirect("Welcome");
        }
    }


	public function index()
	{
        // print_r($this->uri->uri_string());
        // die();
		$data['products'] = $this->product_model->getAll();
        $data['categorys'] = $this->category_model->getAll();
        
		$this->load->view('admin/products/index', $data);
	}

    public function create()
    {
        $product = $this->product_model;
        $product->save();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        redirect('admin/product');
        
    }

    public function edit()
    {
        $product = $this->product_model;

        $product->update();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        

        redirect('admin/product');
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->product_model->delete($id)) {
            $this->session->set_flashdata('success', 'Berhasil dihapus');
            redirect('admin/product');
        }
    }

    
}