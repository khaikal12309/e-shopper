<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
        $this->load->model('user_model');
        $this->load->library('user_agent');
        $this->load->helper('url');
        //cek session dan level user
        if($this->admin->is_role() != "admin")
        {
            $this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
            redirect("Welcome");
        }
    }


	public function index()
	{
        // print_r($this->uri->uri_string());
        // die();
		$data['users'] = $this->user_model->getUser();
		$this->load->view('admin/users/index', $data);
	}

    public function create()
    {
        $user = $this->user_model;

        $check = $user->check();
        
        if ($check == TRUE){
            $user->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect('admin/User');
        }else{
            $this->session->set_flashdata('error', 'Email telah terdaftar');
            redirect('admin/User');
        }
    }

    public function edit()
    {
        $user = $this->user_model;

        $user->update();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        

        redirect('admin/User');
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->user_model->delete($id)) {
            $this->session->set_flashdata('success', 'Berhasil dihapus');
            redirect('admin/User');
        }
    }

    public function updatepass($id){

        $new_password = MD5($this->input->post('new_password'));
        $confirm_password = MD5($this->input->post('confirm_password'));

        if ($new_password != $confirm_password) {
            $this->session->set_flashdata('error', 'Password anda tidak sesuai');
            redirect('admin/User');
        }else{
            $this->password = MD5($this->input->post('new_password'));
            $query = $this->db->update('tb_user', $this, array('id' => $id));
     
            if($query == TRUE){
                $this->session->set_flashdata('success', 'Berhasil diubah');
                redirect($this->agent->referrer());
            }
        }
    }
}