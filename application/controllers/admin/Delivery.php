<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
        $this->load->model('transaksi_model');
        $this->load->model('delivery_model');
        $this->load->helper('url');
        //cek session dan level user
        if($this->admin->is_role() != "admin")
        {
            $this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
            redirect("Welcome");
        }
    }


	public function index()
	{
        $data['deliverys'] = $this->delivery_model->getAll();
        $data['transaksis'] = $this->transaksi_model->getAll();
		$this->load->view('admin/deliverys/index', $data);
	}

    public function create()
    {
        $delivery = $this->delivery_model;
        $delivery->save();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        redirect('admin/delivery');
        
    }

    public function edit()
    {
        $delivery = $this->delivery_model;

        $delivery->update();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        

        redirect('admin/delivery');
    }

    public function confirm($id)
    {
        $delivery = $this->delivery_model;

        $delivery->confirm($id);
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        

        redirect('admin/delivery');
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->delivery_model->delete($id)) {
            $this->session->set_flashdata('success', 'Berhasil dihapus');
            redirect('admin/delivery');
        }
    }

    
}