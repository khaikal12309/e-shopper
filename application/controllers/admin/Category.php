<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
        $this->load->model('category_model');
        $this->load->helper('url');
        //cek session dan level user
        if($this->admin->is_role() != "admin")
        {
            $this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
            redirect("Welcome");
        }
    }


	public function index()
	{
        $data['categorys'] = $this->category_model->getAll();
		$this->load->view('admin/categorys/index', $data);
	}

    public function create()
    {
        $category = $this->category_model;
        $category->save();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        redirect('admin/category');
        
    }

    public function edit()
    {
        $category = $this->category_model;

        $category->update();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        

        redirect('admin/category');
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->category_model->delete($id)) {
            $this->session->set_flashdata('success', 'Berhasil dihapus');
            redirect('admin/category');
        }
    }

    
}