<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
        $this->load->model('toko_model');
        $this->load->helper('url');
        //cek session dan level user
        if($this->admin->is_role() != "admin")
        {
            $this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
            redirect("Welcome");
        }
    }


	public function index()
	{
        $toko['toko'] = $this->toko_model->getToko();
		$this->load->view('admin/toko/index', $toko);
	}

    public function edit()
    {
        $toko = $this->toko_model;

        $toko->update();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        

        redirect('admin/toko');
    }
    
}