<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paid extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
        $this->load->model('transaksi_model');
        $this->load->model('paid_model');
        $this->load->model('delivery_model');
        $this->load->helper('url');
        //cek session dan level user
        if($this->admin->is_role() != "admin")
        {
            $this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
            redirect("Welcome");
        }
    }


	public function index()
	{
        $data['transaksis'] = $this->transaksi_model->getAll();
        $data['paids'] = $this->paid_model->getAll();
		$this->load->view('admin/paids/index', $data);
	}

    public function create()
    {
        $paid = $this->paid_model;
        $paid->save();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        redirect('admin/paid');
        
    }

    public function edit()
    {
        $paid = $this->paid_model;

        $paid->update();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        

        redirect('admin/paid');
    }

    public function confirm()
    {
        $paid = $this->paid_model;

        $paid->confirm();

        $delivery = $this->delivery_model;
        $delivery->save();
        $this->session->set_flashdata('success', 'Berhasil disimpan');
        

        redirect('admin/paid');
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->paid_model->delete($id)) {
            $this->session->set_flashdata('success', 'Berhasil dihapus');
            redirect('admin/paid');
        }
    }

    
}