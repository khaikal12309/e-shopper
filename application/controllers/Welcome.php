<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('product_model');
        $this->load->model('category_model');
        $this->load->model('user_model');
        $this->load->helper('url');
    }
	public function index()
	{
		$data['products'] = $this->product_model->getAll();
		$data['categorys'] = $this->category_model->getAll();
		$this->load->view('home', $data);
	}

	public function show($id)
	{
		$data['products'] = $this->product_model->getByCat($id);
		$data['category'] = $this->category_model->getById($id);
		$data['categorys'] = $this->category_model->getAll();
		$this->load->view('home2', $data);
	}

	public function login()
	{
		$this->load->view('login');
	}

	public function do_register()
	{
		$user = $this->user_model;

        $check = $user->check();
        // print_r($check);
        // die();
        if ($check == TRUE){
            $user->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect('welcome');
        }else{
            $this->session->set_flashdata('error', 'Email telah terdaftar');
            redirect('welcome');
        }
	}
}
