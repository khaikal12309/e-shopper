<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_model extends CI_Model
{
    private $_table = "tb_transaksi";

    public $id;
    
    public function getUn(){

        $this->db->select('tb_transaksi.*, tb_paid.status as status_paid, tb_paid.date as tanggal_bayar');
        $this->db->from('tb_transaksi');
        $this->db->join('tb_paid','tb_paid.transaksi_id = tb_transaksi.id');
        $this->db->where('tb_paid.status',"pending");
        $query_dist=$this->db->get();

        return $query_dist->result();
    }

    public function getSuc(){

        $this->db->select('tb_transaksi.*, tb_delivery.status as status_delivery, tb_delivery.tgl_kirim as tgl_kirim, tb_delivery.tgl_sampai');
        $this->db->from('tb_transaksi');
        $this->db->join('tb_delivery','tb_delivery.transaksi_id = tb_transaksi.id');
        $this->db->where('tb_delivery.status',"done");
        $query_dist=$this->db->get();

        return $query_dist->result();
    }

    public function getAll()
    {
        $this->db->select('tb_transaksi.*, tb_user.name as nama_user, tb_product.name as nama_product');
        $this->db->from('tb_transaksi');
        $this->db->join('tb_user','tb_user.id = tb_transaksi.user_id');
        $this->db->join('tb_product','tb_product.id = tb_transaksi.product_id');
        $query_dist=$this->db->get();
        return $query_dist->result();
    }

    public function getUser($user_id){
        return $this->db->get_where($this->_table, ["user_id" => $user_id])->result();
    }
 

    public function save()
    {

        $post = $this->input->post();

        $this->user_id = $post["user_id"];
        $this->product_id = $post["product_id"];
        $this->qty = $post["qty"];
        $this->delivery_ship = $post["delivery_ship"];
        $this->date = date('Y-m-d H:i:s');
        $product = $this->db->select('tb_product.price')->from('tb_product')->where('id', $post["product_id"])->get()->row();
        $harga = $product->price;
        $price_total = $harga * $post["qty"];
        $this->price_total= $price_total;
        // print_r($harga_product->price);
        // die();
        $this->db->insert($this->_table, $this);
        
    }

    public function update(){
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->user_id = $post["user_id"];
        $this->product_id = $post["product_id"];
        $this->qty = $post["qty"];
        $this->delivery_ship = $post["delivery_ship"];
        $transaksi = $this->db->select('tb_transaksi.qty')->from('tb_transaksi')->where('id', $post["id"])->get()->row();
        $qty = $transaksi->qty;
        if ($qty != $post['qty']){
            $product = $this->db->select('tb_product.price')->from('tb_product')->where('id', $post["product_id"])->get()->row();
            $harga = $product->price;
            $price_total = $harga * $post["qty"];
            $this->price_total= $price_total;
        }
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        
        return $this->db->delete($this->_table, array("id" => $id));
    }

}