<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model
{
    private $_table = "tb_product";

    public $id;
    public $image = "default.png";
    
    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
    }

    public function getAll()
    {
        $this->db->select('tb_product.*, tb_category.name as nama_kategori');
        $this->db->from('tb_product');
        $this->db->join('tb_category','tb_product.category_id = tb_category.id');
        $query_dist=$this->db->get();

        return $query_dist->result();
    }

    public function getByCat($id)
    {
        $this->db->select('tb_product.*, tb_category.name as nama_kategori');
        $this->db->from('tb_product');
        $this->db->join('tb_category','tb_product.category_id = tb_category.id');
        $this->db->where('tb_product.category_id',$id);
        $query_dist=$this->db->get();

        return $query_dist->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {

        $post = $this->input->post();

        $this->name = $post["name"];
        $this->qty = $post["qty"];
        $this->price = $post["price"];
        $this->category_id = $post["category_id"];
        $this->image = $this->_uploadImage();
        $this->descripsi = $post["descripsi"];
        $this->db->insert($this->_table, $this);
        
    }

    public function update(){
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->name = $post["name"];
        $this->qty = $post["qty"];
        $this->price = $post["price"];
        $this->category_id = $post["category_id"];
        if (!empty($_FILES["image"]["name"])) {

            $this->image = $this->_uploadImage();
        } else {
            $this->image = $post["old_image"];
        }
        $this->descripsi = $post["descripsi"];
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        $this->_deleteImage($id);
        
        return $this->db->delete($this->_table, array("id" => $id));
    }
    public function updatestok($barang, $id_barang){
        $this->db->where('tb_barang.id_barang', $id_barang);
        return $this->db->update('tb_barang', $barang);
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './image/product';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = 'img_'.$this->name;
        $config['overwrite']            = true;
        $config['max_size']             = 4000; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('image')) {
            return $this->upload->data("file_name");
        }
        // print_r($this->upload->display_errors());
        return "default.png";
    }

    private function _deleteImage($id)
    {
        $product = $this->getById($id);
        if ($product->image != "default.jpg") {
            $filename = explode(".", $product->image)[0];
            return array_map('unlink', glob(FCPATH."image/product/$filename.*"));
        }
    }
}