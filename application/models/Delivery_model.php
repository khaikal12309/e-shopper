<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery_model extends CI_Model
{
    private $_table = "tb_delivery";

    public $id;

    public function getAll()
    {
        $this->db->select('tb_delivery.*, tb_user.name as nama_user, tb_transaksi.price_total as transaksi_total, tb_product.name as nama_product');
        $this->db->from('tb_delivery');
        $this->db->join('tb_transaksi','tb_transaksi.id = tb_delivery.transaksi_id');
        $this->db->join('tb_user','tb_user.id = tb_transaksi.user_id');
        $this->db->join('tb_product','tb_product.id = tb_transaksi.product_id');
        $query_dist=$this->db->get();
        return $query_dist->result();
    }
 
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }


    public function save()
    {

        $post = $this->input->post();
        $this->transaksi_id = $post["transaksi_id"];
        $this->no_resi = $post["no_resi"];
        $this->kurir = $post["kurir"];
        $this->status = $post["status_delivery"];
        $this->tgl_kirim = date('Y-m-d H:i:s');
        $this->db->insert($this->_table, $this);
        
    }

    public function update(){
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->no_resi = $this->no_resi;
        $this->kurir = $this->kurir;
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function confirm($id){
        $this->status = "done";
        $this->tgl_sampai = date('Y-m-d H:i:s');
        return $this->db->update($this->_table, $this, array('id' => $id));
    }

    public function delete($id)
    {
        
        return $this->db->delete($this->_table, array("id" => $id));
    }

}