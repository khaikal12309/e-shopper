<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Toko_model extends CI_Model
{
    private $_table = "tb_toko";

    public $id;

    public function getToko()
    {
        return $this->db->get($this->_table)->row();
    }


    public function update(){
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->name = $post["name"];
        $this->address = $post["address"];
        $this->transfer_rek = $post["transfer_rek"];
        $this->phone = $post["phone"];
        $this->descripsi = $post["descripsi"];
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

}