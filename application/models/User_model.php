<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $_table = "tb_user";

    public $id;
    public $image = "default.png";
    
    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
    }

    public function getUser()
    {
        return $this->db->get_where($this->_table, ["role" => 'user'])->result();
    }
 
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function check(){
        $post = $this->input->post();
        $email = $post["email"]; 
        // print_r($email);
        // die();     
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('email',$email);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return TRUE;
            
        } else {
            return FALSE;
        }
    }

    public function save()
    {

        $post = $this->input->post();

        $this->name = $post["name"];
        $this->password = MD5($post["password"]);
        $this->email = $post["email"];
        $this->phone = $post["phone"];
        $this->email = $post["email"];
        $this->image = $this->_uploadImage();
        $this->address = $post["address"];
        $this->db->insert($this->_table, $this);
        
    }

    public function update(){
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->name = $post["name"];
        $this->email = $post["email"];
        $this->phone = $post["phone"];
        if (!empty($_FILES["image"]["name"])) {

            $this->image = $this->_uploadImage();
        } else {
            $this->image = $post["old_image"];
        }
        $this->address = $post["address"];
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        $this->_deleteImage($id);
        
        return $this->db->delete($this->_table, array("id" => $id));
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './image/user';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = 'img_'.$this->name;
        $config['overwrite']            = true;
        $config['max_size']             = 4000; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('image')) {
            return $this->upload->data("file_name");
        }
        // print_r($this->upload->display_errors());
        return "default.png";
    }

    private function _deleteImage($id)
    {
        $user = $this->getById($id);
        if ($user->image != "default.jpg") {
            $filename = explode(".", $user->image)[0];
            return array_map('unlink', glob(FCPATH."image/user/$filename.*"));
        }
    }
}