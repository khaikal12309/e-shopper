<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Paid_model extends CI_Model
{
    private $_table = "tb_paid";

    public $id;
    public $image = "default.png";
    
    function __construct(){
        parent::__construct();
          $this->load->helper(array('form', 'url'));
    }

    public function getAll()
    {
        $this->db->select('tb_paid.*, tb_user.name as nama_user, tb_transaksi.price_total as transaksi_total, tb_product.name as nama_product');
        $this->db->from('tb_paid');
        $this->db->join('tb_transaksi','tb_transaksi.id = tb_paid.transaksi_id');
        $this->db->join('tb_user','tb_user.id = tb_transaksi.user_id');
        $this->db->join('tb_product','tb_product.id = tb_transaksi.product_id');
        $query_dist=$this->db->get();
        return $query_dist->result();
    }
 
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }


    public function save()
    {

        $post = $this->input->post();

        $this->transaksi_id = $post["transaksi_id"];
        $this->image = $this->_uploadImage();
        if($this->image != Null){
            $this->status="pending";
        }
        $this->date = date('Y-m-d H:i:s');
        $this->db->insert($this->_table, $this);
        
    }

    public function update(){
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->transaksi_id = $post["transaksi_id"];
        if (!empty($_FILES["image"]["name"])) {

            $this->image = $this->_uploadImage();
        } else {
            $this->image = $post["old_image"];
        }
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function confirm(){
        $post = $this->input->post();
        
        $this->id = $post["id"];
        $paid = $this->getById($id);
        $this->image = $paid->image;
        $this->status = "done";
        if (!empty($_FILES["image"]["name"])) {

            $this->image = $this->_uploadImage();
        } else {
            $this->image = $post["old_image"];
        }
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        $this->_deleteImage($id);
        
        return $this->db->delete($this->_table, array("id" => $id));
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './image/paid';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = 'img_'.$this->name;
        $config['overwrite']            = true;
        $config['max_size']             = 4000; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('image')) {
            return $this->upload->data("file_name");
        }
        // print_r($this->upload->display_errors());
        return "default.png";
    }

    private function _deleteImage($id)
    {
        $paid = $this->getById($id);
        if ($paid->image != "default.jpg") {
            $filename = explode(".", $paid->image)[0];
            return array_map('unlink', glob(FCPATH."image/paid/$filename.*"));
        }
    }
}