<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login | E-Shopper</title>
    <link href="<?php echo base_url();?>assets/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/frontend/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/frontend/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/frontend/css/price-range.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/frontend/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/frontend/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/frontend/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/frontend/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/frontend/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/frontend/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/frontend/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/frontend/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php $this->load->view('layouts/header'); ?>
	
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form method="post" action="<?php echo base_url()?>index.php/login/do_login">
							<input type="email" name="email" placeholder="Email Address" />
							<input type="password" name="password" placeholder="Password" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Keep me signed in
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						<form method="post" enctype="multipart/form-data" action="<?php echo base_url()?>index.php/welcome/do_register">
							<input name="name" type="text" placeholder="Name"/>
							<input name="email" type="email" placeholder="Email Address"/>
							<input name="password" type="password" placeholder="Password"/>
							<input name="phone" type="text" placeholder="Phone"/>
							<input name="image" type="file"/>
							<textarea name="address"></textarea>
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	
		
	<?php $this->load->view('layouts/footer')?>
	<!--/Footer-->
	

  
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/jquery.scrollUp.min.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/price-range.js"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/main.js"></script>
</body>
</html>