<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>E-Shopper | Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/backend/assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/backend/assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sweetalert/dist/sweetalert.css" />
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/backend/assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/frontend/images/home/logo-2.png"/>
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth" style="background:url(<?php echo base_url()?>assets/backend/assets/images/bg.png)no-repeat center center fixed;-webkit-background-size: 100% 100%;
                -moz-background-size: 100% 100%;
                -o-background-size: 100% 100%;
                background-size: 100% 100%;">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="brand-logo">
                  <center>
                    <img src="<?php echo base_url()?>assets/frontend/images/home/logo.png"><br/>
                  </center>
                </div>
                
                <form method="post" action="<?php echo base_url();?>index.php/Login/do_login" class="pt-3">
                  <div class="form-group">
                    <input type="text" name="email" class="form-control form-control-sm" placeholder="Username" required="True">
                  </div>
                  <div class="form-group">
                    <input type="password" name="password" class="form-control form-control-sm" id="exampleInputPassword1" placeholder="Password" required="True">
                  </div>
                  <div class="mt-3">
                    <button class="btn btn-block btn-gradient-primary btn-sm font-weight-medium auth-form-btn" type="submit">SIGN IN</button>
                  </div>
                  <div class="mt-3">
                    <center>
                      <span class="text-muted text-center text-sm-center d-block d-sm-inline-block">Copyright © 2019</span>
                    </center>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?php echo base_url()?>assets/backend/assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<?php echo base_url()?>assets/backend/assets/js/off-canvas.js"></script>
    <script src="<?php echo base_url()?>assets/backend/assets/js/hoverable-collapse.js"></script>
    <script src="<?php echo base_url()?>assets/backend/assets/js/misc.js"></script>
    <script src="<?php echo base_url();?>assets/sweetalert/dist/sweetalert.js"></script>
    <?php if($this->session->flashdata('error')):?>
          <script>
            swal("Oopps!", "<?=$this->session->flashdata('error') ?>", "error");
          </script>
    <?php endif ?>
    <!-- endinject -->
  </body>
</html>