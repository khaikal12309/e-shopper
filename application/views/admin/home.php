<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>E-Shopper | Admin</title>
		<!-- plugins:css -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/assets/vendors/mdi/css/materialdesignicons.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/assets/vendors/css/vendor.bundle.base.css">
		<style type="text/css">
		    .link:hover{
				color: aqua;
			}
			.link:link{
				color: white;
			}
			.link{
				color: white;
			}
			.link:active{
				color: green;
			}
			.link:visited{
				background: yellow;
			}
		</style>
		<!-- endinject -->
		<!-- Plugin css for this page -->
		<!-- End plugin css for this page -->
		<!-- inject:css -->
		<!-- endinject -->
		<!-- Layout styles -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/assets/css/style.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/sweetalert/dist/sweetalert.css" />
		<script src="<?php echo base_url();?>assets/sweetalert/dist/sweetalert.js"></script>
		<!-- End layout styles -->
		<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontend/images/home/logo-2.png"/>
	</head>
	<body>
		<div class="container-scroller">
			<!-- Load Navbar -->
			<?php $this->load->view('admin/navbar')?>
			<!-- partial -->
			<div class="container-fluid page-body-wrapper">
				<!-- Load Sidebar -->
				<?php $this->load->view('admin/sidebar')?>
				<!-- partial -->
				<div class="main-panel">
					<div class="content-wrapper">
						<div class="page-header">
							<h3 class="page-title">
								<span class="page-title-icon bg-gradient-primary text-white mr-2">
									<i class="mdi mdi-home"></i>
								</span> Dashboard </h3>
							<nav aria-label="breadcrumb">
								<ul class="breadcrumb">
									<li class="breadcrumb-item active" aria-current="page">
										<span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
									</li>
								</ul>
							</nav>
						</div>
						<div class="row">
							<div class="col-md-6 stretch-card grid-margin">
								<div class="card bg-gradient-primary card-img-holder text-white">
									<div class="card-body">
										<img src="<?php echo base_url();?>assets/backend/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
										<h4 class="font-weight-normal mb-3">Users <i class="mdi mdi-account-multiple mdi-24px float-right"></i>
										</h4>
										<h2 class="mb-5"><?php echo $count_user;?></h2>
										<h6 class="card-text"><a href="<?php echo base_url();?>index.php/admin/user" class="link"><i class="mdi mdi-eye"></i> Details</h6></a>
									</div>
								</div>
							</div>
							<div class="col-md-6 stretch-card grid-margin">
								<div class="card bg-gradient-info card-img-holder text-white">
									<div class="card-body">
										<img src="<?php echo base_url();?>assets/backend/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
										<h4 class="font-weight-normal mb-3">Product <i class="mdi mdi-store mdi-24px float-right"></i>
										</h4>
										<h2 class="mb-5"><?php echo $count_product;?></h2>
										<h6 class="card-text"><a href="<?php echo base_url();?>index.php/admin/product" class="link"><i class="mdi mdi-eye"></i> Details</h6></a>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 stretch-card grid-margin">
								<div class="card bg-gradient-warning card-img-holder text-white">
									<div class="card-body">
										<img src="<?php echo base_url();?>assets/backend/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
										<h4 class="font-weight-normal mb-3">Unconfir Transaksi<i class="mdi mdi-checkbox-multiple-blank-outline mdi-24px float-right"></i>
										</h4>
										<h2 class="mb-5"><?php echo $count_unconfir;?></h2>
										<h6 class="card-text"><a href="acceptances.html" class="link"><i class="mdi mdi-eye"></i> Details</h6></a>
									</div>
								</div>
							</div>
							<div class="col-md-6 stretch-card grid-margin">
								<div class="card bg-gradient-success card-img-holder text-white">
									<div class="card-body">
										<img src="<?php echo base_url();?>assets/backend/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
										<h4 class="font-weight-normal mb-3">Success Transaksi <i class="mdi mdi-checkbox-multiple-marked-outline mdi-24px float-right"></i>
										</h4>
										<h2 class="mb-5"><?php echo $count_success;?></h2>
										<h6 class="card-text"><a href="outstandings.html" class="link"><i class="mdi mdi-eye"></i> Details</h6></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- content-wrapper ends -->
					<!-- partial:partials/_footer.html -->
					<footer class="footer">
						<div class="d-sm-flex justify-content-center justify-content-sm-between">
							<span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
							<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
						</div>
					</footer>
					<!-- partial -->
				</div>
				<!-- main-panel ends -->
			</div>
			<!-- page-body-wrapper ends -->
		</div>
		<!-- container-scroller -->
		<!-- plugins:js -->
		<script src="<?php echo base_url();?>assets/backend/assets/vendors/js/vendor.bundle.base.js"></script>
		<!-- endinject -->
		<!-- Plugin js for this page -->
		<script src="<?php echo base_url();?>assets/backend/assets/vendors/chart.js/Chart.min.js"></script>
		<!-- End plugin js for this page -->
		<!-- inject:js -->
		<script src="<?php echo base_url();?>assets/backend/assets/js/off-canvas.js"></script>
		<script src="<?php echo base_url();?>assets/backend/assets/js/hoverable-collapse.js"></script>
		<script src="<?php echo base_url();?>assets/backend/assets/js/misc.js"></script>
		<!-- endinject -->
		<!-- Custom js for this page -->
		<script src="<?php echo base_url();?>assets/backend/assets/js/dashboard.js"></script>
		<script src="<?php echo base_url();?>assets/backend/assets/js/todolist.js"></script>
		<!-- End custom js for this page -->

		<?php if($this->session->flashdata('success')):?>
          <script>
            swal("Good job!", "<?=$this->session->flashdata('success') ?>", "success");
          </script>
    <?php endif?>

    <?php if($this->session->flashdata('error')):?>
          <script>
            swal("Oopps!", "<?=$this->session->flashdata('error') ?>", "error");
          </script>
    <?php endif ?>
	</body>
</html>