<!DOCTYPE html>
<html lang="en">
	<head> 
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>E-Shopper | Admin</title>
		<!-- plugins:css -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/assets/vendors/mdi/css/materialdesignicons.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/assets/vendors/css/vendor.bundle.base.css">
		<style type="text/css">
		    .link:hover{
				color: aqua;
			}
			.link:link{
				color: white;
			}
			.link{
				color: white;
			}
			.link:active{
				color: green;
			}
			.link:visited{
				background: yellow;
			}
		</style>
		<!-- endinject -->
		<!-- Plugin css for this page -->
		<!-- End plugin css for this page -->
		<!-- inject:css -->
		<!-- endinject -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatables/css/bootstrap.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatables/css/dataTables.bootstrap4.min.css"/>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/sweetalert/dist/sweetalert.css" />

		<!-- Layout styles -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/assets/css/style.css">
		<!-- End layout styles -->
		<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontend/images/home/logo-2.png"/>
	</head>
	<body>
		<div class="container-scroller">
			<!-- Load Navbar -->
			<?php $this->load->view('admin/navbar')?>
			<!-- partial -->
			<div class="container-fluid page-body-wrapper">
				<!-- Load Sidebar -->
				<?php $this->load->view('admin/sidebar')?>
				<!-- partial -->
				<div class="main-panel">
					<div class="content-wrapper">
						<div class="page-header">
							<h3 class="page-title">
								<span class="page-title-icon bg-gradient-primary text-white mr-2">
									<i class="mdi mdi-database"></i>
								</span> Data Toko 
							</h3>
							<nav aria-label="breadcrumb">
								<ul class="breadcrumb">
									<li class="breadcrumb-item active" aria-current="page">
										<span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
									</li>
								</ul>
							</nav>
						</div>
						<div class="row">
							<div class="col-lg-12 grid-margin stretch-card">
								<div class="card">
									<div class="card-body">
										<form class="forms-sample" method="post" action="<?php echo base_url();?>index.php/admin/Toko/edit">
											<input type="hidden" name="id" value="<?php echo $toko->id ?>" />
											<div class="form-group">
												<label>Nama</label>
												<input type="text" class="form-control" name="name" value="<?php echo $toko->name ?>" required/>
											</div>
											<div class="form-group">
												<label>Telepon</label>
												<input type="text" class="form-control" name="phone" value="<?php echo $toko->phone ?>" required/>
											</div>
											<div class="form-group">
												<label>Alamat</label>
												<textarea class="form-control" name="address"><?php echo $toko->address ?></textarea>
											</div>
											<div class="form-group">
												<label>No Rekening</label>
												<textarea class="form-control" name="transfer_rek"><?php echo $toko->transfer_rek ?></textarea>
											</div>
											<div class="form-group">
												<label>Deskripsi</label>
												<textarea class="form-control" name="descripsi"><?php echo $toko->descripsi ?></textarea>
											</div> 
											<button type="submit" class="btn btn-primary btn-sm">Save changes</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- content-wrapper ends -->
					<!-- partial:partials/_footer.html -->
					<footer class="footer">
						<div class="d-sm-flex justify-content-center justify-content-sm-between">
							<span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
							<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
						</div>
					</footer>
					<!-- partial -->
				</div>
				<!-- main-panel ends -->
			</div>
			<!-- page-body-wrapper ends -->
		</div>
		<!-- container-scroller -->
		<!-- plugins:js -->
		<script src="<?php echo base_url();?>assets/backend/assets/vendors/js/vendor.bundle.base.js"></script>
		<!-- endinject -->
		<!-- Plugin js for this page -->
		<script src="<?php echo base_url();?>assets/backend/assets/vendors/chart.js/Chart.min.js"></script>
		<!-- End plugin js for this page -->
		<!-- inject:js -->
		<script src="<?php echo base_url();?>assets/backend/assets/js/off-canvas.js"></script>
		<script src="<?php echo base_url();?>assets/backend/assets/js/hoverable-collapse.js"></script>
		<script src="<?php echo base_url();?>assets/backend/assets/js/misc.js"></script>
		<!-- endinject -->
		<!-- Custom js for this page -->
		<script src="<?php echo base_url();?>assets/backend/assets/js/dashboard.js"></script>
		<script src="<?php echo base_url();?>assets/backend/assets/js/todolist.js"></script>

		<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/datatables/js/jquery-3.3.1.js"></script>
		<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/datatables/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/datatables/js/dataTables.bootstrap4.min.js"></script>
		<script src="<?php echo base_url();?>assets/sweetalert/dist/sweetalert.js"></script>

		<script type="text/javascript">
			$(document).ready( function () {
			    $('#table_id').DataTable();
			} );
		</script>
		<!-- End custom js for this page -->

		<?php if($this->session->flashdata('success')):?>
          <script>
            swal("Good job!", "<?=$this->session->flashdata('success') ?>", "success");
          </script>
    <?php endif?>

    <?php if($this->session->flashdata('error')):?>
          <script>
            swal("Oopps!", "<?=$this->session->flashdata('error') ?>", "error");
          </script>
    <?php endif ?>
    <!-- End custom js for this page -->
	</body>
</html>