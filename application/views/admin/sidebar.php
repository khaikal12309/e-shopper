<nav class="sidebar sidebar-offcanvas" id="sidebar">
					<ul class="nav">
						<li class="nav-item nav-profile">
							<a href="#" class="nav-link">
								<div class="nav-profile-image">
									<img src="<?php echo base_url();?>assets/backend/assets/images/faces/face1.jpg" alt="profile">
									<span class="login-status online"></span>
									<!--change to offline or busy as needed-->
								</div>
								<div class="nav-profile-text d-flex flex-column">
									<span class="font-weight-bold mb-2">David Grey. H</span>
									<span class="text-secondary text-small">Project Manager</span>
								</div>
								<i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url();?>index.php/admin/home">
								<span class="menu-title">Dashboard</span>
								<i class="mdi mdi-home menu-icon"></i>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
								<span class="menu-title">Transaksi</span>
								<i class="menu-arrow"></i>
								<i class="mdi mdi-autorenew menu-icon"></i>
							</a>
							<div class="collapse" id="ui-basic">
								<ul class="nav flex-column sub-menu">
									<li class="nav-item"> <a class="nav-link" href="<?php echo base_url();?>index.php/admin/transaksi">Transaksi</a></li>
									<li class="nav-item"> <a class="nav-link" href="<?php echo base_url();?>index.php/admin/paid">Pembayaran</a></li>
									<li class="nav-item"> <a class="nav-link" href="<?php echo base_url();?>index.php/admin/delivery">Pengiriman</a></li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php if ($this->uri->uri_string()=='admin/user' ){?>active<?php } ?>" href="<?php echo base_url();?>index.php/admin/user">
								<span class="menu-title">Data User</span>
								<i class="mdi mdi-account-multiple menu-icon"></i>
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if ($this->uri->uri_string()=='admin/product'){?>active<?php } ?>" href="<?php echo base_url()?>index.php/admin/product">
								<span class="menu-title">Data Product</span>
								<i class="mdi mdi-database menu-icon"></i>
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if ($this->uri->uri_string()=='admin/category'){?>active<?php } ?>" href="<?php echo base_url()?>index.php/admin/category">
								<span class="menu-title">Data Category</span>
								<i class="mdi mdi-cards menu-icon"></i>
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if ($this->uri->uri_string()=='admin/toko'){?>active<?php } ?>" href="<?php echo base_url();?>index.php/admin/toko">
								<span class="menu-title">Tentang Toko</span>
								<i class="mdi mdi-store menu-icon"></i>
							</a>
						</li>
					</ul>
				</nav>