<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>E-Shopper | Admin</title>
		<!-- plugins:css -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/assets/vendors/mdi/css/materialdesignicons.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/assets/vendors/css/vendor.bundle.base.css">
		<style type="text/css">
		    .link:hover{
				color: aqua;
			}
			.link:link{
				color: white;
			}
			.link{
				color: white;
			}
			.link:active{
				color: green;
			}
			.link:visited{
				background: yellow;
			}
		</style>
		<!-- endinject -->
		<!-- Plugin css for this page -->
		<!-- End plugin css for this page -->
		<!-- inject:css -->
		<!-- endinject -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatables/css/bootstrap.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatables/css/dataTables.bootstrap4.min.css"/>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/sweetalert/dist/sweetalert.css" />

		<!-- Layout styles -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/assets/css/style.css">
		<!-- End layout styles -->
		<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontend/images/home/logo-2.png"/>
	</head>
	<body>
		<div class="container-scroller">
			<!-- Load Navbar -->
			<?php $this->load->view('admin/navbar')?>
			<!-- partial -->
			<div class="container-fluid page-body-wrapper">
				<!-- Load Sidebar -->
				<?php $this->load->view('admin/sidebar')?>
				<!-- partial -->
				<div class="main-panel">
					<div class="content-wrapper">
						<div class="page-header">
							<h3 class="page-title">
								<span class="page-title-icon bg-gradient-primary text-white mr-2">
									<i class="mdi mdi-database"></i>
								</span> Data Transaksi 
							</h3>
							<nav aria-label="breadcrumb">
								<ul class="breadcrumb">
									<li class="breadcrumb-item active" aria-current="page">
										<span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
									</li>
								</ul>
							</nav>
						</div>
						<div class="row">
							<div class="col-lg-12 grid-margin stretch-card">
								<div class="card">
									<div class="card-header">
										<button type="button" class="btn btn-gradient-success btn-sm" data-toggle="modal" data-target="#tambah_data"><i class="mdi mdi-database-plus icon-lg"></i> Add Transaksi</button>

									</div>
									<div class="card-body">
										<table id="table_id" class="table table-hover table-md" style="width:100%">
											<thead>
												<tr>
													<th>User</th>
													<th>Produk</th>
													<th>QTY</th>
													<th>Price Total</th>
													<th>Date</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($transaksis as $transaksi): ?>
													<tr>
														<td><?php echo $transaksi->nama_user ?></td>
														<td><?php echo $transaksi->nama_product ?></td>
														<td><?php echo $transaksi->qty ?></td>
														<td><?php echo $transaksi->price_total ?></td>
														<td><?php echo $transaksi->date ?></td>
														<td><?php echo $transaksi->status ?></td>
														<td>
															<a class="btn btn-gradient-danger btn-sm" href="<?php echo base_url()."index.php/admin/transaksi/delete/".$transaksi->id?>" onClick="return confirm('Anda yakin akan menghapus data ini?');">
																<i class="mdi mdi-table-row-remove icon-sm"></i>
															</a>

															<button type="button" class="btn btn-gradient-warning btn-sm" data-toggle="modal" data-target="#edit<?php echo $transaksi->id ?>">
																<i class="mdi mdi-lead-pencil icon-sm"/></i>
															</button>
														</td>
													</tr>
												<?php endforeach; ?>  
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- content-wrapper ends -->
					<!-- partial:partials/_footer.html -->
					<footer class="footer">
						<div class="d-sm-flex justify-content-center justify-content-sm-between">
							<span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
							<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
						</div>
					</footer>
					<!-- partial -->
				</div>
				<!-- main-panel ends -->
			</div>
			<!-- page-body-wrapper ends -->
		</div>
		<!-- container-scroller -->
		<!-- plugins:js -->
		<script src="<?php echo base_url();?>assets/backend/assets/vendors/js/vendor.bundle.base.js"></script>
		<!-- endinject -->
		<!-- Plugin js for this page -->
		<script src="<?php echo base_url();?>assets/backend/assets/vendors/chart.js/Chart.min.js"></script>
		<!-- End plugin js for this page -->
		<!-- inject:js -->
		<script src="<?php echo base_url();?>assets/backend/assets/js/off-canvas.js"></script>
		<script src="<?php echo base_url();?>assets/backend/assets/js/hoverable-collapse.js"></script>
		<script src="<?php echo base_url();?>assets/backend/assets/js/misc.js"></script>
		<!-- endinject -->
		<!-- Custom js for this page -->
		<script src="<?php echo base_url();?>assets/backend/assets/js/dashboard.js"></script>
		<script src="<?php echo base_url();?>assets/backend/assets/js/todolist.js"></script>

		<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/datatables/js/jquery-3.3.1.js"></script>
		<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/datatables/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" charset="utf8" src="<?php echo base_url();?>assets/datatables/js/dataTables.bootstrap4.min.js"></script>
		<script src="<?php echo base_url();?>assets/sweetalert/dist/sweetalert.js"></script>

		<script type="text/javascript">
			$(document).ready( function () {
			    $('#table_id').DataTable();
			} );
		</script>
		<!-- End custom js for this page -->

		<?php if($this->session->flashdata('success')):?>
          <script>
            swal("Good job!", "<?=$this->session->flashdata('success') ?>", "success");
          </script>
    <?php endif?>

    <?php if($this->session->flashdata('error')):?>
          <script>
            swal("Oopps!", "<?=$this->session->flashdata('error') ?>", "error");
          </script>
    <?php endif ?>
    <!-- End custom js for this page -->

    <!-- Modal Edit Data -->
	<?php foreach ($transaksis as $transaksi): ?>
		
		<div class="modal fade" id="edit<?php echo $transaksi->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Edit Users</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      	<form class="forms-sample" method="post" action="<?php echo base_url();?>index.php/admin/transaksi/edit" enctype="multipart/form-data">
		      		<input type="hidden" name="id" value="<?php echo $transaksi->id ?>" />
		      		<input type="hidden" name="price_total" value="<?php echo $transaksi->price_total ?>" />
		      		<input type="hidden" name="date" value="<?php echo $transaksi->date ?>" />
		      		<div class="form-group">
		      			<label>User</label>
		      			<select class="form-control form-control-sm" name="user_id">
		      				<option value="<?php echo $transaksi->user_id?>" selected><?php echo $transaksi->nama_user ?></option>
		      				<?php foreach($users as $u): ?>
		      					<?php if($u->id != $transaksi->user_id){ ?>
		      						<option class="form-control" value="<?php echo $u->id?>"><?php echo $u->name?></option>
		      					<?php } ?>
		      				<?php endforeach ?>
		      			</select>
		      		</div>
		      		<div class="form-group">
		      			<label>Product</label>
		      			<select class="form-control form-control-sm" name="product_id">
		      				<option value="<?php echo $transaksi->product_id?>" selected><?php echo $transaksi->nama_product ?></option>
		      				<?php foreach($products as $p): ?>
		      					<?php if($p->id != $transaksi->product_id){ ?>
		      						<option class="form-control" value="<?php echo $p->id?>"><?php echo $p->name?></option>
		      					<?php } ?>
		      				<?php endforeach ?>
		      			</select>
		      		</div>
		      		<div class="form-group">
		      			<label>QTY</label>
		      			<input type="number" class="form-control" name="qty" value="<?php echo $transaksi->qty?>" required/>
		      		</div>
		      		<div class="form-group">
		      			<label>Delivery Ship</label>
		      			<textarea class="form-control" name="delivery_ship"><?php echo $transaksi->delivery_ship?></textarea>
		      		</div> 
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary pull-left btn-sm" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
		        </form>
		      </div>
		    </div>
		  </div>
		</div>

	<?php endforeach ?>
    <!-- Modal Tambah Data -->
	<div class="modal fade" id="tambah_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Tambah Transaksi</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<form method="post" action="<?php echo base_url();?>index.php/admin/transaksi/create" >
		      		<div class="form-group">
		      			<label>User</label>
		      			<select class="form-control form-control-sm" name="user_id">
		      				<option value="" disabled selected hidden>Pilih User</option>
		      				<?php foreach($users as $u): ?>
		      					<option class="form-control" value="<?php echo $u->id?>"><?php echo $u->name?></option>
		      				<?php endforeach ?>
		      			</select>
		      		</div>
		      		<div class="form-group">
		      			<label>Product</label>
		      			<select class="form-control form-control-sm" name="product_id">
		      				<option value="" disabled selected hidden>Pilih Product</option>
		      				<?php foreach($products as $p): ?>
		      					<option class="form-control" value="<?php echo $p->id?>"><?php echo $p->name?></option>
		      				<?php endforeach ?>
		      			</select>
		      		</div>
		      		<div class="form-group">
		      			<label>QTY</label>
		      			<input type="number" class="form-control" name="qty"/>
		      		</div>
		      		<div class="form-group">
		      			<label>Delivery Ship</label>
		      			<textarea class="form-control" name="delivery_ship"></textarea>
		      		</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" >Close</button>
		        <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
		        </form>
	      </div>
	    </div>
	  </div>
	</div>
	</body>
</html>