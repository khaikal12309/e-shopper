<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | E-Shopper</title>
    <link href="<?php echo base_url();?>/assets/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/price-range.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>/assets/frontend/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url();?>/assets/frontend/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/frontend/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php $this->load->view('layouts/header');?>
	
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<form method="post" action="<?php echo base_url();?>index.php/user/homeuser/checkout">
					<input type="hidden" name="user_id" value="<?php echo $this->session->userdata("user_id")?>">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="quantity">Quantity</td>
							<td class="price">Price</td>
							<td class="total">Aksi</td>
						</tr>
	                </thead>
            	
	                <tbody id="detail_cart">
	                	
	                </tbody>

				</table>
				<div class="form-group">
						<label>Shipping address</label>
						<textarea name="delivery_ship"></textarea>
					</div>
				<div class="form-group">
					<button type="submit" class="btn btn-default">Check Out</button>
					</form>
				</div>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label>Shipping address</label>
						<textarea name="delivery_ship"></textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-default check_out">Check Out</button>
					</div>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
	
	<?php $this->load->view('layouts/footer')?>
	<!--/Footer-->
	

  
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/jquery.scrollUp.min.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/price-range.js"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/main.js"></script>

    <script type="text/javascript">
	    $(document).ready(function(){
	        
	        // Load shopping cart
	        $('#detail_cart').load("<?php echo base_url();?>index.php/user/homeuser/load_cart");
	 
	        //Hapus Item Cart
	        $(document).on('click','.hapus_cart',function(){
	            var row_id=$(this).attr("id"); //mengambil row_id dari artibut id
	            $.ajax({
	                url : "<?php echo base_url();?>index.php/user/homeuser/hapus_cart",
	                method : "POST",
	                data : {row_id : row_id},
	                success :function(data){
	                    $('#detail_cart').html(data);
	                }
	            });
	        });
	    });
	</script>
</body>
</html>