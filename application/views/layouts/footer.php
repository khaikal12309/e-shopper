<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="companyinfo">
							<h2><span>e</span>-shopper</h2>
							<p>E Shopper merupakan website e-commerce terbaik didunia saat ini. Menyediakan berbagai macam produk kebutuhan manusia disemua kalangan dengan berbagai diskon dan promo serta harga paling spesial dibanding dengan e-commerce lain</p>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="companyinfo">
							<h2>Follow <span>US</span></h2>
							<img src="<?php echo base_url();?>assets/frontend/images/ig.png" style="width: 5%" />
							<img src="<?php echo base_url();?>assets/frontend/images/wa.png" style="width: 5%" />
							<img src="<?php echo base_url();?>assets/frontend/images/fb.png" style="width: 5%" />
							<img src="<?php echo base_url();?>assets/frontend/images/twt.png" style="width: 5%" />
							<img src="<?php echo base_url();?>assets/frontend/images/tg.png" style="width: 5%" />
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2013 E-SHOPPER Inc. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
				</div>
			</div>
		</div>
		
	</footer>