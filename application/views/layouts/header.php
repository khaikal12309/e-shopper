<header id="header"><!--header-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="<?php echo base_url();?>index.php/welcome/index"><img src="<?php echo base_url();?>assets/frontend/images/home/logo.png" alt="" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<?php if ($this->session->userdata("user_id") != Null){?>
									<li><a href="#"><i class="fa fa-user"></i> Account</a></li>
									<li><a href="<?php echo base_url();?>index.php/user/homeuser/tampil_checkout"><i class="fa fa-crosshairs"></i> Checkout</a></li>
									<li><a href="<?php echo base_url();?>index.php/user/homeuser/tampil_keranjang"><i class="fa fa-shopping-cart"></i> Cart <small><?php echo $this->cart->total_items() ?></small></a></li>
									<li><a href="<?php echo base_url();?>index.php/login/logout"><i class="fa fa-logout"></i> Logout</a></li>
								<?php }else{?>
									<li><a href="<?php echo base_url();?>index.php/welcome/login"><i class="fa fa-lock"></i> Login</a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	</header><!--/header-->