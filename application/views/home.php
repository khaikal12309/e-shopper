<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="<?php echo base_url();?>/assets/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/price-range.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>/assets/frontend/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url();?>/assets/frontend/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php $this->load->view('layouts/header');?>
	
	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1><span>E</span>-SHOPPER</h1>
									<h2>Free E-Commerce Template</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo base_url();?>assets/frontend/images/home/girl1.jpg" class="girl img-responsive" alt="" />
									<img src="<?php echo base_url();?>assets/frontend/images/home/pricing.png"  class="pricing" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="col-sm-6">
									<h1><span>E</span>-SHOPPER</h1>
									<h2>100% Responsive Design</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo base_url();?>assets/frontend/images/home/girl2.jpg" class="girl img-responsive" alt="" />
									<img src="<?php echo base_url();?>assets/frontend/images/home/pricing.png"  class="pricing" alt="" />
								</div>
							</div>
							
							<div class="item">
								<div class="col-sm-6">
									<h1><span>E</span>-SHOPPER</h1>
									<h2>Free Ecommerce Template</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
									<button type="button" class="btn btn-default get">Get it now</button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo base_url();?>assets/frontend/images/home/girl3.jpg" class="girl img-responsive" alt="" />
									<img src="<?php echo base_url();?>assets/frontend/images/home/pricing.png" class="pricing" alt="" />
								</div>
							</div>
							
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
						<?php foreach ($categorys as $cat): ?>
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="<?php echo base_url()."index.php/welcome/show/".$cat->id?>"><?php echo $cat->name;?></a></h4>
								</div>
							</div>
						<?php endforeach ?>
							
						</div><!--/category-products-->
						
					
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Items</h2>
						<?php foreach ($products as $p): ?>
							<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
											<div class="productinfo text-center">
												<img src="<?php echo base_url();?>image/product/<?php echo $p->image?>" alt="" />
												<h2>Rp. <?php echo number_format($p->price,2,',','.');?></h2>
												<p><?php echo $p->name?></p>
												<?php if ($this->session->userdata("user_id") != Null){?>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												<?php } else{?>
													<a href="<?php echo base_url();?>index.php/welcome/login" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Login untuk beli</a>
												<?php } ?>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2>Rp. <?php echo number_format($p->price,2,',','.');?></h2>
													<p><?php echo $p->name?></p>
													<?php if ($this->session->userdata("user_id") != Null){?>
													<a href="<?php echo base_url()."index.php/user/homeuser/tambah_keranjang/".$p->id?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
													<?php } else{?>
														<a href="<?php echo base_url();?>index.php/welcome/login" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Login untuk beli</a>
													<?php } ?>
												</div>
											</div>
									</div>
								</div>
							</div>
						<?php endforeach ?>
					</div><!--features_items-->
					
				</div>
			</div>
		</div>
	</section>
	
	<?php $this->load->view('layouts/footer')?>
	<!--/Footer-->
	

  
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/jquery.scrollUp.min.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/price-range.js"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/main.js"></script>
</body>
</html>