<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | E-Shopper</title>
    <link href="<?php echo base_url();?>/assets/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/price-range.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/frontend/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>/assets/frontend/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url();?>/assets/frontend/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/frontend/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php $this->load->view('layouts/header');?>
	
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="quantity">Quantity</td>
							<td class="price">Price Total</td>
							<td class="price">Delivery Ship</td>
							<td class="price">Status</td>
							<td class="total">Aksi</td>
						</tr>
	                </thead>
            	
	                <tbody>
	                	<?php foreach($transaksis as $t): ?>
	                	<tr>
	                		<td><?php echo $t->product_id ?></td>
	                		<td><?php echo $t->qty ?></td>
	                		<td><?php echo $t->price_total ?></td>
	                		<td><?php echo $t->delivery_ship ?></td>
	                		<td><?php echo $t->status ?></td>
	                		<td></td>
	                	</tr>
	                <?php endforeach ?>
	                	
	                </tbody>

				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label>Shipping address</label>
						<textarea name="delivery_ship"></textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-default check_out">Check Out</button>
					</div>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
	
	<?php $this->load->view('layouts/footer')?>
	<!--/Footer-->
	

  
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/jquery.scrollUp.min.js"></script>
	<script src="<?php echo base_url();?>assets/frontend/js/price-range.js"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/main.js"></script>
</body>
</html>